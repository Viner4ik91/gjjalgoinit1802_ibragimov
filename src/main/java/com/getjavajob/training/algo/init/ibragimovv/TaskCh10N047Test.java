package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N047.findRecursionFibonachchi;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N047Test {
    public static void main(String[] args) {
        testFibonachchi();
    }

    private static void testFibonachchi() {
        assertEquals("TaskCh10N047Test.testFibonachchi", 2, findRecursionFibonachchi(3));
    }
}
