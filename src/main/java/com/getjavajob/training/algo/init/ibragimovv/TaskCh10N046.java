package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N046 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter first number, denominator and last numbers of geometric progression: ");
            int firstN = sc.nextInt();
            int denominator = sc.nextInt();
            int lastN = sc.nextInt();
            int memberN = findGeometricMember(firstN, denominator, lastN);
            int sumMembers = findGeoSum(firstN, denominator, lastN);
            System.out.println("member n = " + memberN);
            System.out.println("sum of first members = " + sumMembers);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int findGeometricMember(int firstN, int denominator, int lastN) {
        return lastN == 1 ? lastN : denominator * findGeometricMember(firstN, denominator, lastN - 1);
    }

    public static int findGeoSum(int firstN, int denominator, int lastN) {
        if (lastN == 0) {
            return lastN;
        } else {
            int f = firstN;

            for (int i = 0; i < lastN - 1; ++i) {
                f *= denominator;
            }
            return f + findGeoSum(firstN, denominator, lastN - 1);
        }
    }
}
