package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh02N013 {
    public static final int HUNDRETS = 100;
    public static final int DECADES = 10;
    public static final int DIVISOR_OF_NUMBER = DECADES;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter tne number > 100 and < 200:");
            int input = sc.nextInt();
            int result = getReverse(input);
            System.out.print("Reverse of number is: " + result);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int getReverse(int input) {
        int firstNumber = input % DIVISOR_OF_NUMBER;
        int secondNumber = input / DECADES % DIVISOR_OF_NUMBER;
        int lastNumber = input / HUNDRETS % DIVISOR_OF_NUMBER;
        return firstNumber * HUNDRETS + secondNumber * DECADES + lastNumber;
    }
}
