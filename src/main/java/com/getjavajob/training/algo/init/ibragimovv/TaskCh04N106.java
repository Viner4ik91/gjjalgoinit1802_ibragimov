package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh04N106 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
        System.out.println("Enter the month(1-12): ");
        int month = sc.nextInt();
        String resultSeason = findSeason(month);
        System.out.println(resultSeason);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static String findSeason(int month) {
        String season;
        switch (month) {
            case 1:
            case 2:
            case 12:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
            default:
                season = "Please, enter 1-12 month";
        }
        return season;
    }
}
