package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

import static java.lang.Math.round;

/**
 * @author vinerI
 */
public class TaskCh05N038 {
    public static final double CONST_FOR_ROUND = 1000.0;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the max step: ");
            int step = sc.nextInt();
            double resultToHome = findDistanceToHome(step);
            double resultAllDistance = findAllDistance(step);
            System.out.println("Distance to Home: " + resultToHome + ",  all distance: " + resultAllDistance);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static double findDistanceToHome(int step) {
        double distanceToHome = 0.0;

        for (int i = 1; i <= step; ++i) {
            distanceToHome += (double) (2 * (i % 2) - 1) / ((double) i + 0.0);
        }
        return (double) round(distanceToHome * CONST_FOR_ROUND) / CONST_FOR_ROUND;
    }

    public static double findAllDistance(int step) {
        double allDistance = 0.0;

        for (int i = 1; i <= step; ++i) {
            allDistance += 1.0 / (i + 0.0);
        }
        return (double) round(allDistance * CONST_FOR_ROUND) / CONST_FOR_ROUND;
    }
}
