package com.getjavajob.training.algo.init.ibragimovv;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

 /**
 * @author vinerI
 */
public class TaskCh13N012 {
    public static void main(String[] args) {
        List<Employee> testEmployees = new ArrayList<>();
        Database db = new Database(testEmployees);
        db.addEmployee(new Employee("Ivanov", "Oleg", "Moscow", 1, 2014));
        db.addEmployee(new Employee("Ivanova", "Anna", "Petriovna", "Kazan", 9, 2011));
        db.addEmployee(new Employee("Sidorov", "Evgeniy", "Eduardovich", "Almetyevsk", 12, 2013));
        db.addEmployee(new Employee("Petrov", "Vladimir", "Vladimirivich", "Ufa", 1, 2017));
        db.addEmployee(new Employee("Putin", "Eduard", "Danilovich", "Pushkino", 5, 2017));
        db.addEmployee(new Employee("Medvedev", "Aslanbek", "Dzamgarovich", "Kirov", 12, 2014));
        db.addEmployee(new Employee("Pushkin", "Alexandr", "Sergeevich", "Tver", 1, 2015));
        db.addEmployee(new Employee("Tolstoy", "Lev", "Ivanovich", "Zelenograd", 7, 2010));
        db.addEmployee(new Employee("Korenina", "Nina", "Andreevna", "Omsk", 12, 2016));
        db.addEmployee(new Employee("Lenin", "Vladimir", "Ilyich", "Zelenodolsk", 8, 2012));
        db.addEmployee(new Employee("Kim", "Chen", "Yn", "Podolsk", 8, 2013));
        db.addEmployee(new Employee("Harlamov", "Garic", "Alekseevich", "Penza", 10, 2016));
        db.addEmployee(new Employee("Batrutdinov", "Timur", "Ibrahimovich", "Ekb", 1, 2017));
        db.addEmployee(new Employee("Trump", "Oliver", "Obamovich", "Murom", 1, 2012));
        db.addEmployee(new Employee("Gusev", "Vasgen", "Oslonbekovich", "Mirnyi", 1, 2016));
        db.addEmployee(new Employee("Akopyan", "Narek", "Michailovich", "Bugulma", 11, 2017));
        db.addEmployee(new Employee("Belov", "Aleksandr", "Sergeevich", "Chistopol", 1, 2015));
        db.addEmployee(new Employee("Dzuba", "Sergey", "Valentinovich", "Zainsk", 1, 2017));
        db.addEmployee(new Employee("Nagiev", "Dmitriy", "Borisovich", "Moscow", 4, 2015));
        db.addEmployee(new Employee("Androidov", "Iphone", "Asusovich", "New York", 8, 2016));

        List<Employee> searchYear = db.searchWorkingMoreYears(3);
        printSearchedEmployees(searchYear);
        System.out.println();
        List<Employee> searchString = db.searchBySubstring("ivan");
        printSearchedEmployees(searchString);
    }

    private static void printSearchedEmployees(List<Employee> employees) {
        for (Employee employee : employees) {
            System.out.println(employee.getSurName() + " " + employee.getName() + " " +
                    employee.getMiddleName() + ", address: " + employee.getAddress() + ", date: " +
                    employee.getMonthWork() + "." + employee.getYearWork());
        }
    }
}

class Employee {
    private String surName;
    private String name;
    private String middleName;
    private String address;
    private int monthWork;
    private int yearWork;

    Employee(String surName, String name, String middleName, String address, int monthWork, int yearWork) {
        this.setSurName(surName);
        this.setName(name);
        this.setMiddleName(middleName);
        this.setAddress(address);
        this.setMonthWork(monthWork);
        this.setYearWork(yearWork);
    }

    Employee(String surName, String middleName, String address, int monthWork, int yearWork) {
        this(surName, middleName, "", address, monthWork, yearWork);
    }

    String getSurName() {
        return surName;
    }

    void setSurName(String surName) {
        this.surName = surName;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getMiddleName() {
        return middleName;
    }

    void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    int getMonthWork() {
        return monthWork;
    }

    void setMonthWork(int monthWork) {
        this.monthWork = monthWork;
    }

    int getYearWork() {
        return yearWork;
    }

    void setYearWork(int yearWork) {
        this.yearWork = yearWork;
    }

    public int howManyYearsWorked() {
        GregorianCalendar today = new GregorianCalendar();
        int yearToday = today.get(Calendar.YEAR);
        int monthToday = today.get(Calendar.MONTH) + 1;

        if (monthToday >= this.getMonthWork()) {
            return yearToday - this.getYearWork();
        } else {
            return yearToday - this.getYearWork() - 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        Employee employee = (Employee) o;
        return getMonthWork() == employee.getMonthWork() && getYearWork() == employee.getYearWork()
                && java.util.Objects.equals(getSurName(), employee.getSurName())
                && java.util.Objects.equals(getName(), employee.getName())
                && java.util.Objects.equals(getMiddleName(), employee.getMiddleName())
                && java.util.Objects.equals(getAddress(), employee.getAddress());
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(getSurName(), getName(), getMiddleName(), getAddress(), getMonthWork(), getYearWork());
    }
}

class Database {
    private List<Employee> employees = new ArrayList<>();

    public Database(List<Employee> employees) {
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public List<Employee> searchWorkingMoreYears(int years) {
        List<Employee> searchedEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.howManyYearsWorked() >= years) {
                searchedEmployees.add(employee);
            }
        }
        return searchedEmployees;
    }

    public List<Employee> searchBySubstring(String substring) {
        List<Employee> searchedEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getSurName().toLowerCase().contains(substring.toLowerCase()) ||
                    employee.getName().toLowerCase().contains(substring.toLowerCase()) ||
                    employee.getMiddleName().toLowerCase().contains(substring.toLowerCase())) {
                searchedEmployees.add(employee);
            }
        }
        return searchedEmployees;
    }
}