package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N045.findMemberN;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N045.findSumOfMembers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N045Test {
    public static void main(String[] args) {
        testFindMemberN();
        testFindSumOfMembers();
    }

    private static void testFindMemberN() {
        assertEquals("TaskCh10N045Test.testFindMemberN", 5, findMemberN(1, 2, 3));
    }

    private static void testFindSumOfMembers() {
        assertEquals("TaskCh10N045Test.testFindSumOfMembers", 9, findSumOfMembers(1, 2, 3));
    }
}
