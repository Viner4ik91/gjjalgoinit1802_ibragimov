package com.getjavajob.training.algo.init.ibragimovv;

import static java.lang.Math.max;

/**
 * @author vinerI
 */
public class TaskCh10N048 {
    public static void main(String[] args) {
        int[] array = new int[]{10, 1, -10, 1, 13, 9};
        int index = array.length;
        int findMax = findMaxElementOfArray(array, index);
        System.out.println(findMax);
    }

    public static int findMaxElementOfArray(int[] array, int index) {
        if (index > 1) {
            return max(array[index - 1], findMaxElementOfArray(array, index - 1));
        }
        return array[0];
    }
}
