package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;

/**
 * @author vinerI
 */
public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] arrayInput = new int[]{1, -3, 4, -5, -6, 7};
        int[] arrayResult = rewriteFirstNegativeSecondPositive(arrayInput);
        System.out.println(Arrays.toString(arrayResult));
    }

    public static int[] rewriteFirstNegativeSecondPositive(int[] arrayInput) {
        int[] arrayResult = new int[arrayInput.length];
        int[] arrayPositive = new int[arrayInput.length];
        int negativeNumbers = 0;
        int positiveNumbers = 0;
        for (int number : arrayInput) {
            if (number < 0) {
                arrayResult[negativeNumbers] = number;
                ++negativeNumbers;
            } else {
                arrayPositive[positiveNumbers] = number;
                ++positiveNumbers;
            }
        }
        System.arraycopy(arrayPositive, 0, arrayResult, negativeNumbers, positiveNumbers);
        return arrayResult;
    }
}
