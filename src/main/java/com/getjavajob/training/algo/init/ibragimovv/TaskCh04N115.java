package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh04N115 {
    public static final int NUMBER_OF_COLORS = 5;
    public static final int YEAR_WE_KNOW_ANIMAL_AND_COLOR = 1984;
    private static final int NUMBER_OF_ANIMALS = 12;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the year: ");
            int year = sc.nextInt();
            String resultColor = findColor(year);
            String resultAnimal = findAnimal(year);
            System.out.print(resultColor + ", " + resultAnimal);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static String findColor(int year) {
        String color = "";
        int colorNumber;
        if (year >= YEAR_WE_KNOW_ANIMAL_AND_COLOR) {
            colorNumber = (year - YEAR_WE_KNOW_ANIMAL_AND_COLOR) / 2 % NUMBER_OF_COLORS;
            switch (colorNumber) {
                case 0:
                    color = "Green";
                    break;
                case 1:
                    color = "Red";
                    break;
                case 2:
                    color = "Yellow";
                    break;
                case 3:
                    color = "White";
                    break;
                case 4:
                    color = "Black";
                    break;
                default:
                    color = "Error!";
            }
        }
        if (year > 0 && year < YEAR_WE_KNOW_ANIMAL_AND_COLOR) {
            colorNumber = (YEAR_WE_KNOW_ANIMAL_AND_COLOR - 1 - year) / 2 % NUMBER_OF_COLORS;
            switch (colorNumber) {
                case 0:
                    color = "Black";
                    break;
                case 1:
                    color = "White";
                    break;
                case 2:
                    color = "Yellow";
                    break;
                case 3:
                    color = "Red";
                    break;
                case 4:
                    color = "Green";
                    break;
                default:
                    color = "Error!";
            }
        }
        return color;
    }

    public static String findAnimal(int year) {
        String animal = "";
        int animalNumber;
        if (year >= YEAR_WE_KNOW_ANIMAL_AND_COLOR) {
            animalNumber = (year - YEAR_WE_KNOW_ANIMAL_AND_COLOR) % NUMBER_OF_ANIMALS;
            switch (animalNumber) {
                case 0:
                    animal = "Rat";
                    break;
                case 1:
                    animal = "Cow";
                    break;
                case 2:
                    animal = "Tiger";
                    break;
                case 3:
                    animal = "Hare";
                    break;
                case 4:
                    animal = "Dragon";
                    break;
                case 5:
                    animal = "Snake";
                    break;
                case 6:
                    animal = "Horse";
                    break;
                case 7:
                    animal = "Sheep";
                    break;
                case 8:
                    animal = "Monkey";
                    break;
                case 9:
                    animal = "Rooster";
                    break;
                case 10:
                    animal = "Dog";
                    break;
                case 11:
                    animal = "Pig";
                    break;
                default:
                    animal = "Error";
            }
        }
        if (year > 0 && year < 1984) {
            animalNumber = (YEAR_WE_KNOW_ANIMAL_AND_COLOR - 1 - year) % NUMBER_OF_ANIMALS;
            switch (animalNumber) {
                case 0:
                    animal = "Pig";
                    break;
                case 1:
                    animal = "Dog";
                    break;
                case 2:
                    animal = "Rooster";
                    break;
                case 3:
                    animal = "Monkey";
                    break;
                case 4:
                    animal = "Sheep";
                    break;
                case 5:
                    animal = "Horse";
                    break;
                case 6:
                    animal = "Snake";
                    break;
                case 7:
                    animal = "Dragon";
                    break;
                case 8:
                    animal = "Hare";
                    break;
                case 9:
                    animal = "Tiger";
                    break;
                case 10:
                    animal = "Cow";
                    break;
                case 11:
                    animal = "Rat";
                    break;
                default:
                    animal = "Error";
            }
        }
        return animal;
    }
}
