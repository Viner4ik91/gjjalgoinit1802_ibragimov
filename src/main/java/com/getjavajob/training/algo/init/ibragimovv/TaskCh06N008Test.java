package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh06N008.findNumbersBeforeN;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh06N008Test {
    public static void main(String[] args) {
        testFindNumbersBeforeN();
    }

    private static void testFindNumbersBeforeN() {
        int[] expected = new int[]{1, 4, 9, 16, 25, 36, 49, 64, 81, 100};
        assertEquals("TaskCh06N008Test.testFindNumbersBeforeN", expected, findNumbersBeforeN(100));
    }
}
