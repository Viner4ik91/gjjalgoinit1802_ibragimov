package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N049.findIndexOfMaxElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N049Test {
    public static void main(String[] args) {
        testIndexOfMax();
    }

    private static void testIndexOfMax() {
        int[] array = new int[]{10, 100, 5, -42, 0, 15};
        int index = array.length - 1;
        assertEquals("TaskCh10N049Test.testIndexOfMax", 1, findIndexOfMaxElement(array, index));
    }
}
