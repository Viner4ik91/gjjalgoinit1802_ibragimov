package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;

/**
 * @author vinerI
 */
public class TaskCh11N158 {
    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 3, 4, 4, 5};
        System.out.println(Arrays.toString(deleteDoublesInArray(array)));
    }

    public static int[] deleteDoublesInArray(int[] array) {
        for (int i = 0; i < array.length; ++i) {
            int newArray = array[i];
            int i1 = i + 1;
            if (i1 < array.length && array[i1] == newArray) {
                System.arraycopy(array, i1 + 1, array, i1, array.length - 1 - i1);
                array[array.length - 1] = 0;
            }
        }

        return array;
    }
}
