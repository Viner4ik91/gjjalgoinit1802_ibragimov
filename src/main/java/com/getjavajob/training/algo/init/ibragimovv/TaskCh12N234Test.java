package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N234.deleteColumn;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N234.deleteString;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteString();
        testDeleteColumn();
    }

    private static void testDeleteString() {
        int[][] arrayInput = new int[][]{
                {12, 26, 12, 10},
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19},
        };
        int[][] expectedArray = new int[][]{
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19},
                {0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.testDeleteString", expectedArray, deleteString(arrayInput, 0));
    }

    private static void testDeleteColumn() {
        int[][] arrayInput = new int[][]{
                {12, 26, 12, 10},
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19}
        };
        int[][] expectedArray = new int[][]{
                {12, 26, 12, 0},
                {20, 21, 14, 0},
                {15, 22, 16, 0},
                {22, 28, 18, 0},
                {26, 29, 20, 0},
                {27, 33, 21, 0},
                {29, 31, 23, 0},
                {26, 34, 27, 0},
                {23, 40, 28, 0},
                {24, 39, 29, 0},
                {22, 45, 21, 0}
        };
        assertEquals("TaskCh12N234Test.testDeleteColumn", expectedArray, deleteColumn(arrayInput, 3));
    }
}
