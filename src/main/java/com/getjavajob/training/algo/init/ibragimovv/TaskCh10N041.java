package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N041 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the number for find factorial: ");
            int number = sc.nextInt();
            int result = findFactorial(number);
            System.out.println("Factorial = " + result);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int findFactorial(int number) {
        return number == 1 ? 1 : findFactorial(number - 1) * number;
    }
}
