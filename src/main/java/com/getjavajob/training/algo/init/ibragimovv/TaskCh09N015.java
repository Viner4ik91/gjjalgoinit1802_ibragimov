package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh09N015 {
    public static void main(String[] args) {
        String word = "TheBestWord!";
        char kSymbol = findKSymbol(word, 5);
        System.out.println("Symbol is - " + kSymbol);
    }

    public static char findKSymbol(String word, int kSymbol) {
        return word.charAt(kSymbol - 1);
    }
}
