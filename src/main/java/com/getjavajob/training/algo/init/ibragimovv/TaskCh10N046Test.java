package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N046.findGeoSum;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N046.findGeometricMember;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N046Test {
    public static void main(String[] args) {
        testFindGeometricMember();
        testFindGeometricSum();
    }

    private static void testFindGeometricMember() {
        assertEquals("TaskCh10N046Test.testFindGeometricMember", 4, findGeometricMember(1, 2, 3));
    }

    private static void testFindGeometricSum() {
        assertEquals("TaskCh10N046Test.testFindGeometricSum", 7, findGeoSum(1, 2, 3));
    }
}
