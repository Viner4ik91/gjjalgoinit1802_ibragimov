package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh04N015 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter month, year today and month, year of birthday: ");
            int monthToday = sc.nextInt();
            int yearToday = sc.nextInt();
            int monthBirthday = sc.nextInt();
            int yearBirthday = sc.nextInt();
            System.out.println("Age - " + determinationAge(monthToday, yearToday, monthBirthday, yearBirthday));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int determinationAge(int monthToday, int yearToday, int monthBirthday, int yearBirthday) {
        return monthToday >= monthBirthday ? yearToday - yearBirthday : yearToday - yearBirthday - 1;
    }
}
