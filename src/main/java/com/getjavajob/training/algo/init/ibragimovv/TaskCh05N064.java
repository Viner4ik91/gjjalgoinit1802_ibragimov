package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh05N064 {
    public static void main(String[] args) {
        int[] inhabitants = new int[]{1000, 300, 200, 100, 600, 200, 250, 120, 420, 300, 500, 100};
        int[] area = new int[]{100, 30, 20, 10, 60, 20, 25, 120, 42, 30, 50, 10};
        System.out.println("Average population density: " + findAverageDensity(inhabitants, area));
    }

    public static double findAverageDensity(int[] inhabitants, int[] area) {
        int averageInhabitants = 0;
        int averageArea = 0;

        for (int i = 0; i < 12; ++i) {
            averageInhabitants = (int) ((double) averageInhabitants + (double) inhabitants[i] + 0.0);
            averageArea = (int) ((double) averageArea + (double) area[i] + 0.0);
        }
        return (double) (averageInhabitants / averageArea);
    }
}
