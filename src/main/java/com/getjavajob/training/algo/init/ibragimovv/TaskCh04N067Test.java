package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N067.findWeekDay;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh04N067Test {
    public static void main(String[] args) {
        testWorkDay();
        testWeekend();
    }

    private static void testWorkDay() {
        assertEquals("TaskCh04N036Test.test1WeekDay", "Workday", findWeekDay(5));
    }

    private static void testWeekend() {
        assertEquals("TaskCh04N036Test.test2WeekDay", "Weekend", findWeekDay(7));
    }
}
