package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh11N158.deleteDoublesInArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh11N158Test {
    public static void main(String[] args) {
        testDeleteDoublesInArray();
    }

    private static void testDeleteDoublesInArray() {
        int[] array = new int[]{1, 3, 3, 4, 4, 5};
        int[] expectedArray = new int[]{1, 3, 4, 5, 0, 0};
        assertEquals("TaskCh11N158Test.testDeleteDoublesInArray", expectedArray, deleteDoublesInArray(array));
    }
}
