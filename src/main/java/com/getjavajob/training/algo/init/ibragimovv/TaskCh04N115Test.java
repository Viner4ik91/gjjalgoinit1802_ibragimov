package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N115.findAnimal;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N115.findColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh04N115Test {
    public static void main(String[] args) {
        testAnimal();
        testColor();
    }

    private static void testAnimal() {
        assertEquals("TaskCh04N106Test.testAnimalAndColor", "Sheep", findAnimal(1991));
    }

    private static void testColor() {
        assertEquals("TaskCh04N106Test.testAnimalAndColor", "White", findColor(1991));
    }
}
