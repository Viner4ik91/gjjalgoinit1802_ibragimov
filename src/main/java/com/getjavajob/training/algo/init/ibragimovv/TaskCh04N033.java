package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh04N033 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the number: ");
            int number = sc.nextInt();
            boolean even = isEven(number);
            System.out.println(even ? "even" : "odd");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }
}
