package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh05N038.findAllDistance;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh05N038.findDistanceToHome;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh05N038Test {
    public static void main(String[] args) {
        testFindDistanceToHome();
        testFindAllDistance();
    }

    private static void testFindDistanceToHome() {
        assertEquals("TaskCh05N038Test.testFindDistanceToHome", 0.688, findDistanceToHome(100));
    }

    private static void testFindAllDistance() {
        assertEquals("TaskCh05N038Test.testFindAllDistance", 5.187, findAllDistance(100));
    }
}
