package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh06N087Test {
    public static void main(String[] args) {
        testResultWinnerFirst();
        testResultWinnerSecond();
        testResultDraw();
    }

    private static void testResultWinnerFirst() {
        Game game = new Game();
        game.giveNameFirstTeam("FirstTeam");
        game.giveNameSecondTeam("SecondTeam");
        game.setScoreFirstTeam(20);
        game.setScoreSecondTeam(1);
        assertEquals("TaskCh06N087Test.testResultWinnerFirst", "FirstTeam is Winners, score: 20 &" +
                " SecondTeam is losers, score: 1", game.result());
    }

    private static void testResultWinnerSecond() {
        Game game = new Game();
        game.giveNameFirstTeam("FirstTeam");
        game.giveNameSecondTeam("SecondTeam");
        game.setScoreFirstTeam(1);
        game.setScoreSecondTeam(20);
        assertEquals("TaskCh06N087Test.testResultWinnerSecond", "SecondTeam is Winners, score: 20 &" +
                " FirstTeam is losers, score: 1", game.result());
    }

    private static void testResultDraw() {
        Game game = new Game();
        game.giveNameFirstTeam("FirstTeam");
        game.giveNameSecondTeam("SecondTeam");
        game.setScoreFirstTeam(1);
        game.setScoreSecondTeam(1);
        assertEquals("TaskCh06N087Test.testResultDraw", "It's draw - 1:1", game.result());
    }
}
