package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N022.getHalfOfWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N022Test {
    public static void main(String[] args) {
        testHalfOfWord();
    }

    private static void testHalfOfWord() {
        assertEquals("TaskCh09N022Test.testHalfOfWord", "TheBestTime", getHalfOfWord("TheBestTimeInMyLife!!!"));
    }
}
