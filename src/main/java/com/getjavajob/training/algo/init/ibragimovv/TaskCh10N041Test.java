package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N041.findFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N041Test {
    public static void main(String[] args) {
        testFindFactorial();
    }

    private static void testFindFactorial() {
        assertEquals("TaskCh10N041Test.tstFindFactorial", 6, findFactorial(3));
    }
}
