package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N042 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the real number and integer power: ");
            double realNumber = sc.nextDouble();
            int nPow = sc.nextInt();
            double result = findPower(realNumber, nPow);
            System.out.println("Factorial = " + result);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static double findPower(double realNumber, int nPow) {
        return nPow == 1 ? realNumber : realNumber * findPower(realNumber, nPow - 1);
    }
}
