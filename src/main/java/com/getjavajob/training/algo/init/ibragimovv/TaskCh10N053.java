package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] arrayInput = new int[]{11, 12, 13, 14, 15, 16};

        for (int i = 0; i < arrayInput.length; ++i) {
            System.out.print(arrayInput[i] + " ");
        }
        System.out.println();
        for (int number : solveReverseArray(arrayInput)) {
            System.out.print(number + " ");
        }
    }

    public static int[] solveReverseArray(int[] arrayInput) {
        findRecursionReverseArray(arrayInput, arrayInput.length - 1, 0);
        return arrayInput;
    }

    private static void findRecursionReverseArray(int[] arrayInput, int lengthOfArray, int firstIndex) {
        if (lengthOfArray >= 0 && firstIndex < arrayInput.length / 2) {
            int saveNumber = arrayInput[firstIndex];
            arrayInput[firstIndex] = arrayInput[lengthOfArray];
            arrayInput[lengthOfArray] = saveNumber;
            findRecursionReverseArray(arrayInput, lengthOfArray - 1, firstIndex + 1);
        }
    }
}
