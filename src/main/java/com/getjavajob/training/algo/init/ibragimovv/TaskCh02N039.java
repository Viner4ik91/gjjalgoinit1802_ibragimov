package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh02N039 {
    private static final int DEGREES_IN_CIRCLE = 360;
    private static final int DEGREES_IN_1H = DEGREES_IN_CIRCLE / 12;
    private static final double DEGREES_IN_1M = DEGREES_IN_1H / 60;
    private static final double DEGREES_IN_1S = DEGREES_IN_1M / 60;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter hour, minute, second: ");
            int hour = sc.nextInt();
            int minute = sc.nextInt();
            int second = sc.nextInt();
            double angle = findAngleBetweenHourHandMidnightAndRealTime(hour, minute, second);
            System.out.println("Angle = " + angle + " degrees");
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static double findAngleBetweenHourHandMidnightAndRealTime(int hour, int minute, int second) {
        return hour * DEGREES_IN_1H + minute * DEGREES_IN_1M + second * DEGREES_IN_1S;
    }
}
