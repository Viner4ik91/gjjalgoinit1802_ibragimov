package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N048.findMaxElementOfArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N048Test {
    public static void main(String[] args) {
        testMaxElement();
    }

    private static void testMaxElement() {
        int[] array = new int[]{10, 100, 5, -42, 2, 15};
        assertEquals("TaskCh10N048Test.testMaxElement", 100, findMaxElementOfArray(array, array.length));
    }
}
