package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N044.findDigitalSqrt;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N044Test {
    public static void main(String[] args) {
        testFindDigitalSqrt();
    }

    private static void testFindDigitalSqrt() {
        assertEquals("TaskCh10N044Test.testFindDigitalSqrt", 1, findDigitalSqrt(22222));
    }
}
