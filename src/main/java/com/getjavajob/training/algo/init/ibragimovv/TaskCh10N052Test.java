package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N052.solveRecursionReverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverse();
    }

    private static void testReverse() {
        assertEquals("TaskCh10N052Test.testReverse", 54321, solveRecursionReverseNumber(12345));
    }
}
