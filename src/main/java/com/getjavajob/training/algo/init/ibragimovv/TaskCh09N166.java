package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh09N166 {
    public static void main(String[] args) {
        String result = swapFirstAndLastWord("John is the best friend of Bob");
        System.out.println(result);
    }

    public static String swapFirstAndLastWord(String string) {
        StringBuilder newString = new StringBuilder();
        String[] SplitString = string.split(" ");
        newString.append(SplitString[SplitString.length - 1]).append(" ");

        for (int i = 1; i < SplitString.length - 1; ++i) {
            newString.append(SplitString[i]).append(" ");
        }

        newString.append(SplitString[0]);
        return newString.toString();
    }
}
