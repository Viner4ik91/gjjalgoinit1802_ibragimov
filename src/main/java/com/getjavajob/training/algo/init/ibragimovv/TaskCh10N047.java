package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N047 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the member of fibonachchi: ");
            int memberOfFibonachchi = sc.nextInt();
            int resultOfMember = findRecursionFibonachchi(memberOfFibonachchi);
            System.out.println(resultOfMember);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int findRecursionFibonachchi(int member) {
        if (member == 0) {
            return member;
        }
        if (member == 1 || member == 2) {
            return 1;
        }
        return findRecursionFibonachchi(member - 1) + findRecursionFibonachchi(member - 2);
    }
}
