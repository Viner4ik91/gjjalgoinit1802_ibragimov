package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N015.determinationAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh04N015Test {
    public static void main(String[] args) {
        test1DeterminationAge();
        test2DeterminationAge();
        test3DeterminationAge();
    }

    private static void test1DeterminationAge() {
        assertEquals("TaskCh04N015.test1DeterminationAge", 29, determinationAge(12, 2014, 6, 1985));
    }

    private static void test2DeterminationAge() {
        assertEquals("TaskCh04N015.test2DeterminationAge", 28, determinationAge(5, 2014, 6, 1985));
    }

    private static void test3DeterminationAge() {
        assertEquals("TaskCh04N015.test3DeterminationAge", 29, determinationAge(6, 2014, 6, 1985));
    }
}
