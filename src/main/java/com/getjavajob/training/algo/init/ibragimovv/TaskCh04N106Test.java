package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N106.findSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh04N106Test {
    public static void main(String[] args) {
        testSeason();
    }

    private static void testSeason() {
        assertEquals("TaskCh04N106Test.testSeason", "Summer", findSeason(7));
    }
}
