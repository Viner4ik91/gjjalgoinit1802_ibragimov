package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh09N042 {
    public static void main(String[] args) {
        String result = getReverseOfTheWord("Ani Lorak");
        System.out.println("After reverse: " + result);
    }

    public static String getReverseOfTheWord(String word) {
        StringBuilder newWord = new StringBuilder(word);
        return newWord.reverse().toString();
    }
}
