package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N055 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the number of decimal system: ");
            int number = sc.nextInt();
            System.out.println("Insert the radix:");
            int radix = sc.nextInt();
            System.out.println(getNumberAnotherSystem(number, radix));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    static String getNumberAnotherSystem(int number, int radix) {
        String[] digit = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        return number == 0 ? "" : getNumberAnotherSystem(number / radix, radix) + digit[number % radix];
    }
}
