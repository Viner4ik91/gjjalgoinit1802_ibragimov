package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N017.checkFirstAndLastSymbols;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N017Test {
    public static void main(String[] args) {
        testCheckTrue();
        testCheckFalse();
    }

    private static void testCheckTrue() {
        assertEquals("TaskCh09N017Test.testCheckTrue", true, checkFirstAndLastSymbols("TheBestWordT"));
    }

    private static void testCheckFalse() {
        assertEquals("TaskCh09N017Test.testCheckFalse", false, checkFirstAndLastSymbols("TheBestWord"));
    }
}
