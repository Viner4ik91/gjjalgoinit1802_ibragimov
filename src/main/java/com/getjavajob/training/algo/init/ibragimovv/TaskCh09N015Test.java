package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N015.findKSymbol;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N015Test {
    public static void main(String[] args) {
        testFindSymbol();
    }

    private static void testFindSymbol() {
        assertEquals("TaskCh09N015Test.testFindSymbol", 'e', findKSymbol("TheBestWord", 3));
    }
}
