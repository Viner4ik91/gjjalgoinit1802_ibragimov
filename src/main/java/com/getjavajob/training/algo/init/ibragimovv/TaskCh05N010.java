package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh05N010 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the exchange course: ");
            double exchangeCourse = sc.nextDouble();
            double[] resultRuble = exchangeDollar(exchangeCourse);
            System.out.println(" $    ruble ");

            for (int i = 0; i < 20; ++i) {
                System.out.println(i + 1 + "   " + resultRuble[i]);
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static double[] exchangeDollar(double exchangeCourse) {
        double[] ruble = new double[20];
        for (int i = 0; i < 20; ++i) {
            ruble[i] = (double) (i + 1) * exchangeCourse;
        }
        return ruble;
    }
}
