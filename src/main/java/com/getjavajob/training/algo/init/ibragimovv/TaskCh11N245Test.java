package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh11N245.rewriteFirstNegativeSecondPositive;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh11N245Test {
    public static void main(String[] args) {
        testRewriteArray();
    }

    private static void testRewriteArray() {
        int[] array = new int[]{1, -3, 3, 4, -4, -5};
        int[] expected = new int[]{-3, -4, -5, 1, 3, 4};
        assertEquals("TaskCh11N245Test.testRewriteArray", expected, rewriteFirstNegativeSecondPositive(array));
    }
}
