package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh09N185 {
    public static void main(String[] args) {
        String arithmetic = findWrongParenthesis("((a + b) * c) / d)) + e");
        System.out.println(arithmetic);
    }

    public static String findWrongParenthesis(String arString) {
        String result = "";
        int k = 0;
        int m = 0;

        for (int i = 0; i < arString.length(); ++i) {
            switch (arString.charAt(i)) {
                case '(':
                    ++k;
                    break;
                case ')':
                    --k;
                    m = i;
            }
        }
        if (k < 0) {
            result = "no, the first wrong right ')' at position - " + (m - 1);
        }
        if (k == 0) {
            result = "yes, all brackets is correct";
        }
        if (k > 0) {
            result = "no, there are " + k + " unnecessary left '(' brackets";
        }
        return result;
    }
}
