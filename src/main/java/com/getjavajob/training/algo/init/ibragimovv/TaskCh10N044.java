package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N044 {
    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        try (Scanner sc = new Scanner(System.in)) {
            int number = sc.nextInt();
            int digSqrt = findDigitalSqrt(number);
            System.out.println("Digital root is - " + digSqrt);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    private static int findSum(int number) {
        return number == 0 ? number : number % 10 + findSum(number / 10);
    }

    public static int findDigitalSqrt(int number) {
        return number < 10 ? number : findDigitalSqrt(findSum(number));
    }
}
