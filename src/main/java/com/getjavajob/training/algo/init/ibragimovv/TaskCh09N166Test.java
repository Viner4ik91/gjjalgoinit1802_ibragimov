package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N166.swapFirstAndLastWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N166Test {
    public static void main(String[] args) {
        testSwapFirstAndLastWord();
    }

    private static void testSwapFirstAndLastWord() {
        assertEquals("TaskCh09N166Test.testSwapFirstAndLastWord", "Bob is the best friend of John",
                swapFirstAndLastWord("John is the best friend of Bob"));
    }
}
