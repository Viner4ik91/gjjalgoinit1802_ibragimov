package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh10N050 {
    public static void main(String[] args) {
        try {
            int resultOfAkkerman = findAkkermanRecursion(4, 2);
            System.out.println("Value of the Akkerman function = " + resultOfAkkerman);
        } catch (StackOverflowError var2) {
            System.out.println("StackOverflow exception");
        }
    }

    public static int findAkkermanRecursion(int numberN, int numberM) {
        if (numberN == 0) {
            return numberM + 1;
        }
        if (numberN > 0 && numberM == 0) {
            return findAkkermanRecursion(numberN - 1, 1);
        } else {
            return findAkkermanRecursion(numberN - 1, findAkkermanRecursion(numberN, numberM - 1));
        }
    }
}
