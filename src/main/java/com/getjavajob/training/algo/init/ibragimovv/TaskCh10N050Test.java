package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N050.findAkkermanRecursion;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N050Test {
    public static void main(String[] args) {
        testFindAkkermanRecursion();
    }

    private static void testFindAkkermanRecursion() {
        assertEquals("TaskCh10N050Test.testFindAkkermanRecursion", 3, findAkkermanRecursion(1, 1));
    }
}
