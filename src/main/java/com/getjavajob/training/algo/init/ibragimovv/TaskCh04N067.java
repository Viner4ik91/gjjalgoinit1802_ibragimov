package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh04N067 {
    public static final int DAYS_IN_WEEK = 7;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the day of the week:");
            int day = sc.nextInt();
            String weekDay = findWeekDay(day);
            System.out.println(weekDay);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static String findWeekDay(int day) {
        return day % DAYS_IN_WEEK <= 5 && day % DAYS_IN_WEEK >= 1 ? "Workday" : "Weekend";
    }
}
