package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh09N022 {
    public static void main(String[] args) {
        String result = getHalfOfWord("TheBestTimeInMyLife!!!");
        System.out.println("Half of the word - " + result);
    }

    public static String getHalfOfWord(String word) {
        return word.substring(0, word.length() / 2);
    }
}
