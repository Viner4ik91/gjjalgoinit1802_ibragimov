package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh02N031 {
    public static final int HUNDREDS = 100;
    public static final int DECADES = 10;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter tne number >= 100 and <= 999:");
            int input = sc.nextInt();
            int result = moveSecondNumberToTheEnd(input);
            System.out.print("Number X= " + result);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int moveSecondNumberToTheEnd(int input) {
        int firstNumber = input / HUNDREDS;
        int secondNumber = input % 10;
        int lastNumber = input / DECADES % 10;
        return firstNumber * HUNDREDS + secondNumber * DECADES + lastNumber;
    }
}
