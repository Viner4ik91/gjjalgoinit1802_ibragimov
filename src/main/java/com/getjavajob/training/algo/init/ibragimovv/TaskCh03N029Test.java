package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh03N029.checkAllNegative;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh03N029.checkAllOdd;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh03N029.checkOneMoreHundred;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh03N029.checkOneZero;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh03N029.checkOnlyOneLessTwenty;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh03N029.checkOnlyOneMultipleFive;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh03N029Test {
    public static void main(String[] args) {
        testAllOddTrue();
        testAllOddFalse();
        testOnlyOneLessTwentyTrue();
        testOnlyOneLessTwentyFalse();
        testOneZeroTrue();
        testOneZeroFalse();
        testAllNegativeTrue();
        testAllNegativeFalse();
        testOnlyOneMultipleFiveTrue();
        testOnlyOneMultipleFiveFalse();
        testOneMoreHundredTrue();
        testOneMoreHundredFalse();
    }

    private static void testAllOddTrue() {
        assertEquals("TaskCh03N029.testAllOddTrue", true, checkAllOdd(13, 17));
        assertEquals("TaskCh03N029.testAllOddTrue", true, checkAllOdd(17, 13));
    }

    private static void testAllOddFalse() {
        assertEquals("TaskCh03N029.testAllOddFalseOneOdd", false, checkAllOdd(22, 9));
        assertEquals("TaskCh03N029.testAllOddFalseZeroOdd", false, checkAllOdd(22, 20));
    }

    private static void testOnlyOneLessTwentyTrue() {
        assertEquals("TaskCh03N029.testOnlyOneLessTwentyTrue", true, checkOnlyOneLessTwenty(21, 5));
        assertEquals("TaskCh03N029.testOnlyOneLessTwentyTrue", true, checkOnlyOneLessTwenty(5, 21));
    }

    private static void testOnlyOneLessTwentyFalse() {
        assertEquals("TaskCh03N029.testOnlyOneLessTwentyFalseZeroLess", false, checkOnlyOneLessTwenty(21, 40));
        assertEquals("TaskCh03N029.testOnlyOneLessTwentyFalseTwoLess", false, checkOnlyOneLessTwenty(2, 4));
    }

    private static void testOneZeroTrue() {
        assertEquals("TaskCh03N029.testOneZeroTrueOneZero", true, checkOneZero(0, 5));
        assertEquals("TaskCh03N029.testOneZeroTrueTwoZero", true, checkOneZero(0, 0));
        assertEquals("TaskCh03N029.testOneZeroTrueTwoZero", true, checkOneZero(5, 0));
    }

    private static void testOneZeroFalse() {
        assertEquals("TaskCh03N029.testOneZeroFalse", false, checkOneZero(2, 5));
    }

    private static void testAllNegativeTrue() {
        assertEquals("TaskCh03N029.testAllNegativeTrue", true, checkAllNegative(-1, -2, -3));
    }

    private static void testAllNegativeFalse() {
        assertEquals("TaskCh03N029.testAllNegativeFalseOnePositive", false, checkAllNegative(1, -2, -3));
        assertEquals("TaskCh03N029.testAllNegativeFalseOnePositive", false, checkAllNegative(-1, 2, -3));
        assertEquals("TaskCh03N029.testAllNegativeFalseOnePositive", false, checkAllNegative(-1, -2, 3));
        assertEquals("TaskCh03N029.testAllNegativeFalseTwoPositive", false, checkAllNegative(1, 2, -3));
        assertEquals("TaskCh03N029.testAllNegativeFalseTwoPositive", false, checkAllNegative(-1, 2, 3));
        assertEquals("TaskCh03N029.testAllNegativeFalseTwoPositive", false, checkAllNegative(1, -2, 3));
        assertEquals("TaskCh03N029.testAllNegativeFalseThreePositive", false, checkAllNegative(1, 2, 3));
    }

    private static void testOnlyOneMultipleFiveTrue() {
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveTrue", true, checkOnlyOneMultipleFive(10, 12, 31));
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveTrue", true, checkOnlyOneMultipleFive(12, 10, 31));
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveTrue", true, checkOnlyOneMultipleFive(31, 12, 10));
    }

    private static void testOnlyOneMultipleFiveFalse() {
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveFalseTwoMulti", false, checkOnlyOneMultipleFive(10, 15, 31));
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveFalseTwoMulti", false, checkOnlyOneMultipleFive(10, 31, 15));
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveFalseTwoMulti", false, checkOnlyOneMultipleFive(31, 10, 15));
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveFalseZeroMulti", false, checkOnlyOneMultipleFive(11, 1, 3));
        assertEquals("TaskCh03N029.testOnlyOneMultipleFiveFalseAllMulti", false, checkOnlyOneMultipleFive(10, 15, 30));
    }

    private static void testOneMoreHundredTrue() {
        assertEquals("TaskCh03N029.testOneMoreHundredTrueOneMore", true, checkOneMoreHundred(101, 12, 31));
        assertEquals("TaskCh03N029.testOneMoreHundredTrueOneMore", true, checkOneMoreHundred(12, 101, 31));
        assertEquals("TaskCh03N029.testOneMoreHundredTrueOneMore", true, checkOneMoreHundred(31, 12, 101));
        assertEquals("TaskCh03N029.testOneMoreHundredTrueTwoMore", true, checkOneMoreHundred(101, 120, 31));
        assertEquals("TaskCh03N029.testOneMoreHundredTrueTwoMore", true, checkOneMoreHundred(31, 120, 101));
        assertEquals("TaskCh03N029.testOneMoreHundredTrueTwoMore", true, checkOneMoreHundred(101, 31, 120));
        assertEquals("TaskCh03N029.testOneMoreHundredTrueThreeMore", true, checkOneMoreHundred(101, 120, 310));
    }

    private static void testOneMoreHundredFalse() {
        assertEquals("TaskCh03N029.testOneMoreHundredFalse", false, checkOneMoreHundred(1, 12, 31));
    }
}