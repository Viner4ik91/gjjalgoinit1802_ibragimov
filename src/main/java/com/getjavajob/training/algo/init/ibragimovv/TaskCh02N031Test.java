package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh02N031.moveSecondNumberToTheEnd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh02N031Test {
    public static void main(String[] args) {
        testMovingNumber();
    }

    private static void testMovingNumber() {
        assertEquals("TaskCh02N031.testMovingNumber", 567, moveSecondNumberToTheEnd(576));
    }
}
