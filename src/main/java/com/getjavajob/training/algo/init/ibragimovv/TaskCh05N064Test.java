package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh05N064.findAverageDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh05N064Test {
    public static void main(String[] args) {
        testFindAverageDensity();
    }

    private static void testFindAverageDensity() {
        int[] inhabitants = new int[]{1000, 300, 200, 80, 600, 200, 200, 120, 400, 300, 500, 100};
        int[] area = new int[]{100, 30, 20, 10, 60, 20, 20, 110, 40, 30, 50, 10};
        assertEquals("TaskCh05N038Test.testFindDistanceToHome", 8, findAverageDensity(inhabitants, area));
    }
}
