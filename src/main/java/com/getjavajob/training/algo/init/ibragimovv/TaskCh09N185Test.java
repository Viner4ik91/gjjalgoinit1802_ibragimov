package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N185.findWrongParenthesis;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N185Test {
    public static void main(String[] args) {
        testFindWrongRightParenthesis();
        testFindAllCorrectParenthesis();
        testFindWrongLeftParenthesis();
    }

    private static void testFindWrongRightParenthesis() {
        assertEquals("TaskCh09N185Test.testFindWrongRightParenthesis", "no, the first wrong right ')' at position - 7",
                findWrongParenthesis("(a + b)))"));
    }

    private static void testFindAllCorrectParenthesis() {
        assertEquals("TaskCh09N185Test.testFindAllCorrectParenthesis", "yes, all brackets is correct",
                findWrongParenthesis("(a + b) * c"));
    }

    private static void testFindWrongLeftParenthesis() {
        assertEquals("TaskCh09N185Test.testFindWrongLeftParenthesis", "no, there are 3 unnecessary left '(' brackets",
                findWrongParenthesis("((((a + b)"));
    }
}
