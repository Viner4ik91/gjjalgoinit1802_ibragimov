package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N107.replaceFirstAWithLastO;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N107Test {
    public static void main(String[] args) {
        testReplaceAO();
    }

    private static void testReplaceAO() {
        assertEquals("TaskCh09N107Test.testReplaceAO", "getJovaJab", replaceFirstAWithLastO("getJavaJob"));
    }
}
