package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N043.findCountOfDigits;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N043.findSumOfDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N043Test {
    public static void main(String[] args) {
        testFindCountOfDigits();
        testFindSumOfDigits();
    }

    private static void testFindCountOfDigits() {
        assertEquals("TaskCh10N043Test.testFindCountOfDigits", 5, findCountOfDigits(22222));
    }

    private static void testFindSumOfDigits() {
        assertEquals("TaskCh10N043Test.testFindSumOfDigits", 10, findSumOfDigits(22222));
    }
}
