package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh02N043.reviewDivision;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh02N043Test {
    public static void main(String[] args) {
        testReviewDivision();
    }

    private static void testReviewDivision() {
        assertEquals("TaskCh02N043.testReviewDivision", 1, reviewDivision(20, 2));
    }
}
