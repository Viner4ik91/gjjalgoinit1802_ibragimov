package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N052 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the number to reverse: ");
            int number = sc.nextInt();
            System.out.println(number);
            System.out.println(solveRecursionReverseNumber(number));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int solveRecursionReverseNumber(int number) {
        return findRecursionReverseNumber(number, 0);
    }

    private static int findRecursionReverseNumber(int number, int reverseNumber) {
        return number > 0 ? findRecursionReverseNumber(number / 10, reverseNumber * 10 + number % 10) : reverseNumber;
    }
}
