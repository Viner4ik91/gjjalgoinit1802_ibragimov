package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N045 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter first number, difference and last numbers of arithmetic progression: ");
            int firstN = sc.nextInt();
            int dif = sc.nextInt();
            int lastN = sc.nextInt();
            int memberN = findMemberN(firstN, dif, lastN);
            int sumMembers = findSumOfMembers(firstN, dif, lastN);
            System.out.println("member n = " + memberN);
            System.out.println("sum of first members = " + sumMembers);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int findMemberN(int firstN, int dif, int lastN) {
        return lastN == 1 ? lastN : firstN * dif + findMemberN(firstN, dif, lastN - 1);
    }

    public static int findSumOfMembers(int firstN, int dif, int lastN) {
        return lastN == 0 ? lastN : firstN + dif * (lastN - 1) + findSumOfMembers(firstN, dif, lastN - 1);
    }
}
