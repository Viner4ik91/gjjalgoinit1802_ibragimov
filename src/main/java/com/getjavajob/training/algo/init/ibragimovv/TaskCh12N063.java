package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;

/**
 * @author vinerI
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] arrayClasses = new int[][]{
                {12, 26, 12, 10},
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19},
        };
        int[] result = findAverageNumberOfStudents(arrayClasses);
        System.out.println(Arrays.toString(result));
    }

    public static int[] findAverageNumberOfStudents(int[][] arrayClasses) {
        int[] resultArray = new int[arrayClasses[0].length];
        for (int j = 0; j < arrayClasses[0].length; ++j) {
            int sum = 0;
            for (int[] array : arrayClasses) {
                sum += array[j];
            }
            resultArray[j] = sum / arrayClasses.length;
        }
        return resultArray;
    }
}
