package com.getjavajob.training.algo.init.ibragimovv;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * @author vinerI
 */
public class TaskCh01N017 {
    public static void main(String[] args) {
        double resultO = solveO(1);
        double resultP = solveP(2, 1, 0, 1);
        double resultR = solveR(1);
        double resultS = solveS(1);
        System.out.println(resultO);
        System.out.println(resultP);
        System.out.println(resultR);
        System.out.println(resultS);
    }

    /**
     * Solve square root of 1 - sin^2x
     */
    private static double solveO(int x) {
        return sqrt(1.0 - pow(sin((double) x), 2.0));
    }

    /**
     * Solve 1 / sqrt(a * x ^ 2 + b * x + c)
     */
    private static double solveP(int a, int b, int c, int x) {
        return 1.0 / sqrt((double) a * pow((double) x, 2.0) + (double) (b * x) + (double) c);
    }

    /**
     * Solve (sqrt(x + 1) + sqrt(x - 1)) / 2 * sqrt(x)
     */
    private static double solveR(int x) {
        return (sqrt((double) (x + 1)) + sqrt((double) (x - 1))) / 2.0 * sqrt((double) x);
    }

    /**
     * Solve |x| + |x + 1|
     */
    private static double solveS(int x) {
        return (double) (abs(x) + abs(x + 1));
    }
}
