package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N033.isEven;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh04N033Test {
    public static void main(String[] args) {
        testEven();
        testOdd();
    }

    private static void testEven() {
        assertEquals("TaskCh04N033Test.testEven", true, isEven(2));
    }

    private static void testOdd() {
        assertEquals("TaskCh04N033Test.testOdd", false, isEven(1));
    }
}
