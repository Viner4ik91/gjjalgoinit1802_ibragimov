package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh12N023 {
    public static void main(String[] args) {
        int size = inputSize();
        print(solveArrayTaskA(size));
        print(solveArrayTaskB(size));
        print(solveArrayTaskV(size));
    }

    private static int inputSize() {
        int size = 0;
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the odd size of arrays: ");
            size = sc.nextInt();
            if (size % 2 == 1) {
                return size;
            } else {
                System.out.println("Size must be odd: ");
                inputSize();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return size;
    }

    public static int[][] solveArrayTaskA(int size) {
        int[][] arrayResult = new int[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                arrayResult[i][j] = 0;
                if (i == j) {
                    arrayResult[i][j] = 1;
                }

                if (i == size - 1 - j) {
                    arrayResult[i][j] = 1;
                }
            }
        }
        return arrayResult;
    }

    public static int[][] solveArrayTaskB(int size) {
        int[][] arrayResult = new int[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                arrayResult[i][j] = 0;
                if (i == j) {
                    arrayResult[i][j] = 1;
                }

                if (i == size - 1 - j) {
                    arrayResult[i][j] = 1;
                }

                if (i == size / 2 | j == size / 2) {
                    arrayResult[i][j] = 1;
                }
            }
        }
        return arrayResult;
    }

    public static int[][] solveArrayTaskV(int size) {
        int[][] arrayResult = new int[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = i; j < size - i; ++j) {
                arrayResult[i][j] = 1;
                arrayResult[size - i - 1][j] = 1;
            }
        }
        return arrayResult;
    }

    private static void print(int[][] arrayResult) {
        System.out.println(Arrays.deepToString(arrayResult));
    }
}
