package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

import static java.lang.Math.sqrt;

/**
 * @author vinerI
 */
public class TaskCh06N008 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter n: ");
            int n = sc.nextInt();
            int[] result = findNumbersBeforeN(n);

            for (int i = 0; i < result.length; i++) {
                int number = result[i];
                System.out.println(number);
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int[] findNumbersBeforeN(int n) {
        int lastNumber = (int) sqrt((double) n);
        int[] arrayNumbers = new int[lastNumber];

        for (int i = 0; i < lastNumber; ++i) {
            arrayNumbers[i] = (i + 1) * (i + 1);
        }
        return arrayNumbers;
    }
}
