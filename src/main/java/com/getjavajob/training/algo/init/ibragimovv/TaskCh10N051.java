package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh10N051 {
    public static void main(String[] args) {
        System.out.print("a) ");
        solveRecursionA(5);
        System.out.println();
        System.out.print("b) ");
        solveRecursionB(5);
        System.out.println();
        System.out.print("v) ");
        solveRecursionV(5);
    }

    private static void solveRecursionA(int n) {
        if (n > 0) {
            System.out.print(n);
            solveRecursionA(n - 1);
        }
    }

    private static void solveRecursionB(int n) {
        if (n > 0) {
            solveRecursionB(n - 1);
            System.out.print(n);
        }
    }

    private static void solveRecursionV(int n) {
        if (n > 0) {
            System.out.print(n);
            solveRecursionV(n - 1);
            System.out.print(n);
        }
    }
}
