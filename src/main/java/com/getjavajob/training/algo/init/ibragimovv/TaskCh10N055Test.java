package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N055.getNumberAnotherSystem;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N055Test {
    public static void main(String[] args) {
        testGetNumberAnotherSystem();
    }

    private static void testGetNumberAnotherSystem() {
        assertEquals("TaskCh10N055Test.testGetNumberAnotherSystem", "10", getNumberAnotherSystem(16, 16));
    }
}
