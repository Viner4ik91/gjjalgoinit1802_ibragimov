package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh09N017 {
    public static void main(String[] args) {
        boolean result = checkFirstAndLastSymbols("TheBestWordT");
        System.out.println("Is first letter equal last?: " + result);
    }

    public static boolean checkFirstAndLastSymbols(String word) {
        return word.charAt(0) == word.charAt(word.length() - 1);
    }
}
