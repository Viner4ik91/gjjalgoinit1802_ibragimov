package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh10N049 {
    public static void main(String[] args) {
        int[] array = new int[]{20, 1, -10, 1, 111, 190};
        int index = array.length - 1;
        int maxIndex = findIndexOfMaxElement(array, index);
        System.out.println("Index of max element: " + maxIndex);
    }

    public static int findIndexOfMaxElement(int[] array, int index) {
        int indexOfMax = index;
        if (index > 0 && array[index] < array[findIndexOfMaxElement(array, index - 1)]) {
            indexOfMax = findIndexOfMaxElement(array, index - 1);
        }
        return indexOfMax;
    }
}
