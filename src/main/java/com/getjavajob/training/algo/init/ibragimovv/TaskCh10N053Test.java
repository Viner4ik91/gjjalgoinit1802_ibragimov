package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N053.solveReverseArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N053Test {
    public static void main(String[] args) {
        testSolveReverseArray();
    }

    private static void testSolveReverseArray() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] expected = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        assertEquals("TaskCh10N053Test.testSolveReverseArray", expected, solveReverseArray(array));
    }
}
