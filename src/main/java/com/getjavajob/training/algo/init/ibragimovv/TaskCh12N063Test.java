package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N063.findAverageNumberOfStudents;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh12N063Test {
    public static void main(String[] args) {
        testAverageNumber();
    }

    private static void testAverageNumber() {
        int[][] arrayInput = new int[][]{
                {12, 26, 12, 10},
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19},
        };
        int[] expectedArray = new int[]{22, 31, 20, 16};

        assertEquals("TaskCh12N063Test.testAverageNumber", expectedArray, findAverageNumberOfStudents(arrayInput));
    }
}
