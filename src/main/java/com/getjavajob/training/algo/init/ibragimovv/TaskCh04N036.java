package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh04N036 {
    public static final int TIME_OF_GREEN = 3;
    public static final int TIME_OF_RED = 2;
    public static final int ALL_TIME = TIME_OF_GREEN + TIME_OF_RED;

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the time in minutes: ");
            int time = sc.nextInt();
            String color = findColor(time);
            System.out.println("The color is - " + color);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static String findColor(int time) {
        return time % ALL_TIME < 3 ? "green" : "red";
    }
}
