package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N023.solveArrayTaskA;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N023.solveArrayTaskB;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N023.solveArrayTaskV;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh12N023Test {
    public static void main(String[] args) {
        testSolveTaskA();
        testSolveTaskB();
        testSolveTaskV();
    }

    private static void testSolveTaskA() {
        int[][] expectedArray = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };

        assertEquals("TaskCh12N023Test.testSolveTaskA", expectedArray, solveArrayTaskA(7));
    }

    private static void testSolveTaskB() {
        int[][] expectedArray = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };

        assertEquals("TaskCh12N023Test.testSolveTaskB", expectedArray, solveArrayTaskB(7));
    }

    private static void testSolveTaskV() {
        int[][] expectedArray = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };

        assertEquals("TaskCh12N023Test.testSolveTaskV", expectedArray, solveArrayTaskV(7));
    }
}
