package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh01N003 {
    public static void main(String[] args) {
        int input = scanInput();
        System.out.println("Yor number is - " + input);
    }

    private static int scanInput() {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the number, please:");
            int x = sc.nextInt();
            return x;
        }
    }
}
