package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N042.findPower;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N042Test {
    public static void main(String[] args) {
        testFindPower();
    }

    private static void testFindPower() {
        assertEquals("TaskCh10N042Test.testFindPower", 16, findPower(4.0, 2));
    }
}
