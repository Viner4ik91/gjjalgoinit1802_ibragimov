package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N056 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the number to test for prime: ");
            int number = sc.nextInt();
            System.out.println("Is " + number + " a prime number?: " + checkPrime(number));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static boolean checkPrime(int number) {
        int divisor = 2;
        return recursionCheckPrime(number, divisor);
    }

    private static boolean recursionCheckPrime(int number, int divisor) {
        if (number < 2) {
            return false;
        } else if (divisor > number / 2) {
            return true;
        } else if (number % divisor == 0) {
            return false;
        } else {
            ++divisor;
            return recursionCheckPrime(number, divisor + 1);
        }
    }
}
