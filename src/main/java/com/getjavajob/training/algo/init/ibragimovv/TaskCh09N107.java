package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh09N107 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the word: ");
            String word = sc.nextLine();
            String result = replaceFirstAWithLastO(word);
            System.out.println("After changing first 'a' letter for the last 'o': " + result);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static String replaceFirstAWithLastO(String word) {
        StringBuilder newWord = new StringBuilder(word);
        int firstA = newWord.indexOf("a");
        int lastB = newWord.lastIndexOf("o");
        if (firstA <= 0 && lastB <= 0) {
            newWord = new StringBuilder("Can't find 'a' and 'o' letters");
        } else {
            newWord.deleteCharAt(firstA);
            newWord.insert(firstA, "o");
            newWord.deleteCharAt(lastB);
            newWord.insert(lastB, "a");
        }
        return newWord.toString();
    }
}
