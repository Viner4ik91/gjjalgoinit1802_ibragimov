package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh05N010.exchangeDollar;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh05N010Test {
    public static void main(String[] args) {
        testExchangingDollar();
    }

    private static void testExchangingDollar() {
        double[] expected = new double[]{10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0,
                110.0, 120.0, 130.0, 140.0, 150.0, 160.0, 170.0, 180.0, 190.0, 200.0};
        assertEquals("TaskCh05N010Test.testExchangingDollar", expected, exchangeDollar(10.0));
    }
}
