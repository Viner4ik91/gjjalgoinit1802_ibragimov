package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        try (Scanner sc = new Scanner(System.in)) {
            int number = sc.nextInt();
            int resultSum = findSumOfDigits(number);
            int resultCount = findCountOfDigits(number);
            System.out.println("Sum of numbers: " + resultSum);
            System.out.println("Count of numbers: " + resultCount);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int findSumOfDigits(int number) {
        return number == 0 ? number : number % 10 + findSumOfDigits(number / 10);
    }

    public static int findCountOfDigits(int number) {
        return number == 0 ? number : 1 + findCountOfDigits(number / 10);
    }
}
