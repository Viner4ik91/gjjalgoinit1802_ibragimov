package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh02N013.getReverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverse();
    }

    private static void testReverse() {
        assertEquals("TaskCh02N013.testReverse", 456, getReverse(654));
    }
}
