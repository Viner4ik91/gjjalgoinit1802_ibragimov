package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh12N028 {
    public static void main(String[] args) {
        int size = inputSize();

        for (int[] i : solveArray(size)) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println();
    }

    private static int inputSize() {
        int size = 0;
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the odd size of arrays: ");
            size = sc.nextInt();
            if (size % 2 == 1) {
                return size;
            } else {
                System.out.println("Size must be odd: ");
                inputSize();
                return size;
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return size;
    }

    public static int[][] solveArray(int size) {
        int[][] arrayResult = new int[size][size];
        int count = 1;

        for (int j = 1; j <= size / 2 + 1; ++j) {
            int i;
            for (i = j - 1; i < size - j + 1; ++i) {
                arrayResult[j - 1][i] = count++;
            }

            for (i = j; i < size - j + 1; ++i) {
                arrayResult[i][size - j] = count++;
            }

            for (i = size - j - 1; i >= j - 1; --i) {
                arrayResult[size - j][i] = count++;
            }

            for (i = size - j - 1; i >= j; --i) {
                arrayResult[i][j - 1] = count++;
            }
        }
        return arrayResult;
    }
}
