package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh02N043 {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter numbers A & B: ");
            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = reviewDivision(a, b);
            System.out.println("Result of division = " + c);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static int reviewDivision(int a, int b) {
        return a % b * (b % a) + 1;
    }
}
