package com.getjavajob.training.algo.init.ibragimovv;

/**
 * @author vinerI
 */
public class TaskCh03N029 {
    public static final int DIVISOR = 5;

    public static void main(String[] args) {
        int x = -10;
        int y = 20;
        int z = -30;
        System.out.println(checkAllOdd(x, y));
        System.out.println(checkOnlyOneLessTwenty(x, y));
        System.out.println(checkOneZero(x, y));
        System.out.println(checkAllNegative(x, y, z));
        System.out.println(checkOnlyOneMultipleFive(x, y, z));
        System.out.println(checkOneMoreHundred(x, y, z));
    }

    /**
     * Check that x and y is odd
     *
     * @param x
     * @param y
     * @return boolean result
     */
    public static boolean checkAllOdd(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * Check that only one of parameters is less than twenty
     *
     * @param x
     * @param y
     * @return boolean result
     */
    public static boolean checkOnlyOneLessTwenty(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * Check at least one of the parameters is zero
     *
     * @param x
     * @param y
     * @return boolean result
     */
    public static boolean checkOneZero(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * Check all of parameters is negative
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static boolean checkAllNegative(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * Check that only one of the parameters is a multiple of five
     *
     * @param x
     * @param y
     * @param z
     * @return boolean result
     */
    public static boolean checkOnlyOneMultipleFive(int x, int y, int z) {
        boolean firstExpression = x % DIVISOR == 0 && y % DIVISOR != 0 && z % DIVISOR != 0;
        boolean secondExpression = x % DIVISOR != 0 && y % DIVISOR == 0 && z % DIVISOR != 0;
        boolean  thirdExpression = x % DIVISOR != 0 && y % DIVISOR != 0 && z % DIVISOR == 0;
        return firstExpression || secondExpression || thirdExpression;
    }

    /**
     * Check at least one of parameters more than hundred
     *
     * @param x
     * @param y
     * @param z
     * @return boolean result
     */
    public static boolean checkOneMoreHundred(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
