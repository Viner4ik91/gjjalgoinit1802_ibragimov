package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N024.solveArrayTaskA;
import static com.getjavajob.training.algo.init.ibragimovv.TaskCh12N024.solveArrayTaskB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh12N024Test {
    public static void main(String[] args) {
        testSolveTaskA();
        testSolveTaskB();
    }

    private static void testSolveTaskA() {
        int[][] expectedArray = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };

        assertEquals("TaskCh12N024Test.testSolveTaskA", expectedArray, solveArrayTaskA(6));
    }

    private static void testSolveTaskB() {
        int[][] expectedArray = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}
        };

        assertEquals("TaskCh12N024Test.testSolveTaskB", expectedArray, solveArrayTaskB(6));
    }
}
