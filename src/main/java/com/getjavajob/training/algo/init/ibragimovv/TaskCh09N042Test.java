package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh09N042.getReverseOfTheWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverseWord();
    }

    private static void testReverseWord() {
        assertEquals("TaskCh09N042Test.testReverseWord", "karoLinA", getReverseOfTheWord("AniLorak"));
    }
}
