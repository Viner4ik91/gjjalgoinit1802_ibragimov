package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh04N036.findColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh04N036Test {
    public static void main(String[] args) {
        test1Color();
        test2Color();
    }

    private static void test1Color() {
        assertEquals("TaskCh04N036Test.test1Color", "red", findColor(3));
    }

    private static void test2Color() {
        assertEquals("TaskCh04N036Test.test2Color", "green", findColor(5));
    }
}
