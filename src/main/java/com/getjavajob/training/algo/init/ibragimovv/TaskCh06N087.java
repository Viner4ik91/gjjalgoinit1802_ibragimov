package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.teams();
        game.play();
    }
}

class Game {
    Scanner s = new Scanner(System.in);
    private String teamFirst;
    private String teamSecond;
    private int scoreFirstTeam;
    private int scoreSecondTeam;

    public void teams() {
        System.out.print("Enter team #1: ");
        giveNameFirstTeam(s.nextLine());
        System.out.print("Enter team #2: ");
        giveNameSecondTeam(s.nextLine());
    }

    private String takeNameFirstTeam() {
        return teamFirst;
    }

    public void giveNameFirstTeam(String teamFirst) {
        this.teamFirst = teamFirst;
    }

    private String takeNameSecondTeam() {
        return this.teamSecond;
    }

    public void giveNameSecondTeam(String teamSecond) {
        this.teamSecond = teamSecond;
    }

    public void play() {
        System.out.print("Enter team to score(1 or 2 or 0 to finish game): ");
        int team = this.s.nextInt();
        if (team >= 0 && team <= 2) {
            if (team == 1) {
                this.scanScoreFirstTeam();
            }
            if (team == 2) {
                this.scanScoreSecondTeam();
            }
            if (team == 0) {
                this.printResult();
            }
        } else {
            System.out.println("Enter just 1, 2 commands or 0 to finish game" + team);
            this.play();
        }
    }

    private void scanScoreFirstTeam() {
        System.out.print("Enter score (1 or 2 or 3):");
        int score = this.s.nextInt();
        if (score == 0) {
            this.printResult();
        } else {
            this.countScoreFirstTeam(score);
            this.printScore();
            this.play();
        }
    }

    private void scanScoreSecondTeam() {
        System.out.print("Enter score (1 or 2 or 3):");
        int score = this.s.nextInt();
        if (score == 0) {
            this.printResult();
        } else {
            this.countScoreSecondTeam(score);
            this.printScore();
            this.play();
        }
    }

    private void countScoreFirstTeam(int score) {
        if (score >= 1 && score <= 3) {
            score += this.getScoreFirstTeam();
            setScoreFirstTeam(score);
        } else {
            System.out.println("Just 1-3 points, try again");
        }
    }

    private void countScoreSecondTeam(int score) {
        if (score >= 1 && score <= 3) {
            score += this.getScoreSecondTeam();
            setScoreSecondTeam(score);
        } else {
            System.out.println("Just 1-3 points, try again");
        }
    }

    private int getScoreFirstTeam() {
        return scoreFirstTeam;
    }

    public void setScoreFirstTeam(int scoreFirstTeam) {
        this.scoreFirstTeam = scoreFirstTeam;
    }

    private int getScoreSecondTeam() {
        return scoreSecondTeam;
    }

    public void setScoreSecondTeam(int scoreSecondTeam) {
        this.scoreSecondTeam = scoreSecondTeam;
    }

    String score() {
        return "Intermediate score: " + this.getScoreFirstTeam() + ":" + this.getScoreSecondTeam();
    }

    String result() {
        String result = "";
        if (getScoreFirstTeam() > getScoreSecondTeam()) {
            result = takeNameFirstTeam() + " is Winners, score: " + getScoreFirstTeam() + " & " +
                    takeNameSecondTeam() + " is losers, score: " + getScoreSecondTeam();
        } else if (getScoreFirstTeam() < getScoreSecondTeam()) {
            result = takeNameSecondTeam() + " is Winners, score: " + getScoreSecondTeam() + " & " +
                    takeNameFirstTeam() + " is losers, score: " + getScoreFirstTeam();
        } else if (getScoreFirstTeam() == getScoreSecondTeam()) {
            result = "It's draw - " + getScoreFirstTeam() + ":" + getScoreSecondTeam();
        }
        return result;
    }

    private void printScore() {
        System.out.println(score());
    }

    private void printResult() {
        System.out.println(result());
    }
}
