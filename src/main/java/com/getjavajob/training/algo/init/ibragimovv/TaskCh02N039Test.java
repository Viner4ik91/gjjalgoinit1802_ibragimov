package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh02N039.findAngleBetweenHourHandMidnightAndRealTime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh02N039Test {
    public static void main(String[] args) {
        testAngle();
    }

    private static void testAngle() {
        assertEquals("TaskCh02N039.testFindPhi", 90, findAngleBetweenHourHandMidnightAndRealTime(3, 0, 0));
    }
}
