package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh12N234 {
    public static void main(String[] args) {
        int[][] stringArray = {
                {12, 26, 12, 10},
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19},
        };
        int[][] columnArray = {
                {12, 26, 12, 10},
                {20, 21, 14, 11},
                {15, 22, 16, 21},
                {22, 28, 18, 12},
                {26, 29, 20, 23},
                {27, 33, 21, 24},
                {29, 31, 23, 12},
                {26, 34, 27, 15},
                {23, 40, 28, 17},
                {24, 39, 29, 18},
                {22, 45, 21, 19},
        };
        int stringToDelete = inputIntegers();
        int columnToDelete = inputIntegers();
        printResult(deleteString(stringArray, stringToDelete));
        System.out.println();
        printResult(deleteColumn(columnArray, columnToDelete));
    }

    private static int inputIntegers() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of deleting string and column: ");
        int integer = sc.nextInt();
        return integer;
    }

    public static int[][] deleteString(int[][] array, int string) {
        int j;
        for (j = 0; j < array[0].length; ++j) {
            array[string][j] = 0;
        }

        for (j = 0; j < array[0].length; ++j) {
            for (int i = 0; i < array.length - 1; ++i) {
                if (array[i][j] == 0) {
                    array[i][j] = array[i + 1][j];
                    array[i + 1][j] = 0;
                }
            }
        }
        return array;
    }

    public static int[][] deleteColumn(int[][] array, int column) {
        int j;
        for (j = 0; j < array.length; ++j) {
            array[j][column] = 0;
        }

        for (j = 0; j < array[0].length - 1; ++j) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i][j] == 0) {
                    array[i][j] = array[i][j + 1];
                    array[i][j + 1] = 0;
                }
            }
        }
        return array;
    }

    private static void printResult(int[][] resultArray) {
        for (int[] i : resultArray) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println();
    }

}
