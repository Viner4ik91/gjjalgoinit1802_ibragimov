package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author vinerI
 */
public class TaskCh12N024 {
    public static void main(String[] args) {
        int size = inputSize();
        print(solveArrayTaskA(size));
        print(solveArrayTaskB(size));
    }

    private static int inputSize() {
        int size = 0;
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the odd size of arrays: ");
            size = sc.nextInt();
        } catch (Exception e) {
            e.getStackTrace();
        }
        return size;
    }

    public static int[][] solveArrayTaskA(int size) {
        int[][] arrayResult = new int[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                if (i == 0 || j == 0) {
                    arrayResult[i][j] = 1;
                } else {
                    arrayResult[i][j] = arrayResult[i - 1][j] + arrayResult[i][j - 1];
                }
            }
        }
        return arrayResult;
    }

    public static int[][] solveArrayTaskB(int size) {
        int[][] arrayResult = new int[size][size];
        for (int i = 0; i < size; ++i) {
            int counter = i;

            for (int j = 0; j < size; ++j) {
                ++counter;
                if (counter > size) {
                    counter = 1;
                }
                arrayResult[i][j] = counter;
            }
        }
        return arrayResult;
    }

    private static void print(int[][] resultArray) {
        for (int[] i : resultArray) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println();
    }
}
