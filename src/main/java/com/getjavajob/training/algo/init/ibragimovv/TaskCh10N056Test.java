package com.getjavajob.training.algo.init.ibragimovv;

import static com.getjavajob.training.algo.init.ibragimovv.TaskCh10N056.checkPrime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh10N056Test {
    public static void main(String[] args) {
        testCheckPrime();
        testCheckComposite();
    }

    private static void testCheckPrime() {
        assertEquals("TaskCh10N056Test.testCheckPrime", true, checkPrime(15));
    }

    private static void testCheckComposite() {
        assertEquals("TaskCh10N056Test.testCheckComposite", false, checkPrime(16));
    }
}
