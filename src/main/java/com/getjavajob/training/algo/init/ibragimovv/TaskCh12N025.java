package com.getjavajob.training.algo.init.ibragimovv;

import java.util.Arrays;

/**
 * @author vinerI
 */
public class TaskCh12N025 {
    public static void main(String[] args) {
        print(solveArrayTaskA(10, 12));
        print(solveArrayTaskB(10, 12));
        print(solveArrayTaskV(10, 12));
        print(solveArrayTaskG(10, 12));
        print(solveArrayTaskD(10, 12));
        print(solveArrayTaskE(10, 12));
        print(solveArrayTaskJ(10, 12));
        print(solveArrayTaskZ(10, 12));
        print(solveArrayTaskI(10, 12));
        print(solveArrayTaskK(10, 12));
        print(solveArrayTaskL(10, 12));
        print(solveArrayTaskM(10, 12));
        print(solveArrayTaskN(10, 12));
        print(solveArrayTaskO(10, 12));
        print(solveArrayTaskP(10, 12));
        print(solveArrayTaskR(10, 12));
    }

    private static void print(int[][] array) {
        for (int[] i : array) {
            System.out.println(Arrays.toString(i));
        }
        System.out.println();
    }

    private static int[][] solveArrayTaskA(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int i1 = 1;
        for (int i = 0; i < sizeJ; ++i) {
            for (int j = 0; j < sizeI; ++i1) {
                array[i][j] = i1;
                ++j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskB(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int j = 0; j < sizeI; ++j) {
            for (int i = 0; i < sizeJ; ++counter) {
                array[i][j] = counter;
                ++i;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskV(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = 0; i < sizeJ; ++i) {
            for (int j = sizeI - 1; j >= 0; ++counter) {
                array[i][j] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskG(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = 0; i < sizeI; ++i) {
            for (int j = sizeJ - 1; j >= 0; ++counter) {
                array[j][i] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskD(int sizeI, int sizeJ) {
        int[][] array = new int[sizeI][sizeJ];
        int counter = 1;
        for (int i = 0; i < sizeI; ++i) {
            int j;
            for (j = 0; j < sizeJ; ++counter) {
                array[i][j] = counter;
                ++j;
            }
            ++i;
            for (j = sizeI + 1; j >= 0; ++counter) {
                array[i][j] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskE(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = 0; i < sizeI; ++i) {
            int j;
            for (j = 0; j < sizeJ; ++counter) {
                array[j][i] = counter;
                ++j;
            }
            ++i;
            for (j = sizeI + 1; j >= 0; ++counter) {
                array[j][i] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskJ(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeJ - 1; i >= 0; --i) {
            for (int j = 0; j < sizeI; ++counter) {
                array[i][j] = counter;
                ++j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskZ(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeI - 1; i >= 0; --i) {
            for (int j = 0; j < sizeJ; ++counter) {
                array[j][i] = counter;
                ++j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskI(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeJ - 1; i >= 0; --i) {
            for (int j = sizeI - 1; j >= 0; ++counter) {
                array[i][j] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskK(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeI - 1; i >= 0; --i) {
            for (int j = sizeJ - 1; j >= 0; ++counter) {
                array[j][i] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskL(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeI + 1; i >= 0; --i) {
            int j;
            for (j = 0; j < sizeI; ++counter) {
                array[i][j] = counter;
                ++j;
            }
            --i;
            for (j = sizeI - 1; j >= 0; ++counter) {
                array[i][j] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskM(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = 0; i < sizeJ; ++i) {
            int j;
            for (j = sizeI - 1; j >= 0; ++counter) {
                array[i][j] = counter;
                --j;
            }
            ++i;
            for (j = 0; j < sizeI; ++counter) {
                array[i][j] = counter;
                ++j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskN(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeI - 1; i >= 0; --i) {
            int j;
            for (j = 0; j < sizeJ; ++counter) {
                array[j][i] = counter;
                ++j;
            }
            --i;
            for (j = sizeI + 1; j >= 0; ++counter) {
                array[j][i] = counter;
                --j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskO(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = 0; i < sizeI; ++i) {
            int j;
            for (j = sizeI + 1; j >= 0; ++counter) {
                array[j][i] = counter;
                --j;
            }
            ++i;
            for (j = 0; j < sizeJ; ++counter) {
                array[j][i] = counter;
                ++j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskP(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeI + 1; i >= 0; --i) {
            int j;
            for (j = sizeI - 1; j >= 0; ++counter) {
                array[i][j] = counter;
                --j;
            }
            --i;
            for (j = 0; j < sizeI; ++counter) {
                array[i][j] = counter;
                ++j;
            }
        }
        return array;
    }

    private static int[][] solveArrayTaskR(int sizeI, int sizeJ) {
        int[][] array = new int[sizeJ][sizeI];
        int counter = 1;
        for (int i = sizeI - 1; i >= 0; --i) {
            int j;
            for (j = sizeI + 1; j >= 0; ++counter) {
                array[j][i] = counter;
                --j;
            }
            --i;
            for (j = 0; j < sizeJ; ++counter) {
                array[j][i] = counter;
                ++j;
            }
        }
        return array;
    }
}
