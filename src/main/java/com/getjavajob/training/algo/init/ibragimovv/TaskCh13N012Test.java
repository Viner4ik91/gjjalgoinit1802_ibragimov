package com.getjavajob.training.algo.init.ibragimovv;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class TaskCh13N012Test {
    public static void main(String[] args) {
        testBySubstring();
        testHowMuchYears();
        testMoreYears();
    }

    private static void testBySubstring() {
        List<Employee> testEmployees = new ArrayList<>();
        Database db = new Database(testEmployees);
        db.addEmployee(new Employee("Ivanov", "Oleg", "Moscow", 1, 2014));
        db.addEmployee(new Employee("Ivanova", "Anna", "Petriovna", "Kazan", 9, 2011));
        db.addEmployee(new Employee("Sidorov", "Evgeniy", "Eduardovich", "Almetyevsk", 12, 2013));
        db.addEmployee(new Employee("Petrov", "Vladimir", "Vladimirivich", "Ufa", 1, 2017));
        db.addEmployee(new Employee("Putin", "Eduard", "Danilovich", "Pushkino", 5, 2017));
        db.addEmployee(new Employee("Medvedev", "Aslanbek", "Dzamgarovich", "Kirov", 12, 2014));
        db.addEmployee(new Employee("Pushkin", "Alexandr", "Sergeevich", "Tver", 1, 2015));
        db.addEmployee(new Employee("Tolstoy", "Lev", "Ivanovich", "Zelenograd", 7, 2010));
        db.addEmployee(new Employee("Korenina", "Nina", "Andreevna", "Omsk", 12, 2016));
        db.addEmployee(new Employee("Lenin", "Vladimir", "Ilyich", "Zelenodolsk", 8, 2012));
        db.addEmployee(new Employee("Kim", "Chen", "Yn", "Podolsk", 8, 2013));
        db.addEmployee(new Employee("Harlamov", "Garic", "Alekseevich", "Penza", 10, 2016));
        db.addEmployee(new Employee("Batrutdinov", "Timur", "Ibrahimovich", "Ekb", 1, 2017));
        db.addEmployee(new Employee("Trump", "Oliver", "Obamovich", "Murom", 1, 2012));
        db.addEmployee(new Employee("Gusev", "Vasgen", "Oslonbekovich", "Mirnyi", 1, 2016));
        db.addEmployee(new Employee("Akopyan", "Narek", "Michailovich", "Bugulma", 11, 2017));
        db.addEmployee(new Employee("Belov", "Aleksandr", "Sergeevich", "Chistopol", 1, 2015));
        db.addEmployee(new Employee("Dzuba", "Sergey", "Valentinovich", "Zainsk", 1, 2017));
        db.addEmployee(new Employee("Nagiev", "Dmitriy", "Borisovich", "Moscow", 4, 2015));
        db.addEmployee(new Employee("Androidov", "Iphone", "Asusovich", "New York", 8, 2016));
        List<Employee> expectedListString = new ArrayList<>();
        expectedListString.add(new Employee("Ivanov", "Oleg", "Moscow", 1, 2014));
        expectedListString.add(new Employee("Ivanova", "Anna", "Petriovna", "Kazan", 9, 2011));
        expectedListString.add(new Employee("Tolstoy", "Lev", "Ivanovich", "Zelenograd", 7, 2010));
        assertEquals("TaskCh13N12Test.testBySubstring", expectedListString, db.searchBySubstring("Ivan"));
    }

    private static void testHowMuchYears() {
        Employee human = new Employee("Petrov", "Vladimir", "Vladimirivich", "Ufa", 1, 2017);
        int expected = human.howManyYearsWorked();
        assertEquals("TaskCh13N12Test.testHowMuchYears", expected, 1);
    }

    private static void testMoreYears() {
        List<Employee> testEmployees = new ArrayList<>();
        Database db = new Database(testEmployees);
        db.addEmployee(new Employee("Ivanov", "Oleg", "Moscow", 1, 2014));
        db.addEmployee(new Employee("Ivanova", "Anna", "Petriovna", "Kazan", 9, 2011));
        db.addEmployee(new Employee("Sidorov", "Evgeniy", "Eduardovich", "Almetyevsk", 12, 2013));
        db.addEmployee(new Employee("Petrov", "Vladimir", "Vladimirivich", "Ufa", 1, 2017));
        db.addEmployee(new Employee("Putin", "Eduard", "Danilovich", "Pushkino", 5, 2017));
        db.addEmployee(new Employee("Medvedev", "Aslanbek", "Dzamgarovich", "Kirov", 12, 2014));
        db.addEmployee(new Employee("Pushkin", "Alexandr", "Sergeevich", "Tver", 1, 2015));
        db.addEmployee(new Employee("Tolstoy", "Lev", "Ivanovich", "Zelenograd", 7, 2010));
        db.addEmployee(new Employee("Korenina", "Nina", "Andreevna", "Omsk", 12, 2016));
        db.addEmployee(new Employee("Lenin", "Vladimir", "Ilyich", "Zelenodolsk", 8, 2012));
        db.addEmployee(new Employee("Kim", "Chen", "Yn", "Podolsk", 8, 2013));
        db.addEmployee(new Employee("Harlamov", "Garic", "Alekseevich", "Penza", 10, 2016));
        db.addEmployee(new Employee("Batrutdinov", "Timur", "Ibrahimovich", "Ekb", 1, 2017));
        db.addEmployee(new Employee("Trump", "Oliver", "Obamovich", "Murom", 1, 2012));
        db.addEmployee(new Employee("Gusev", "Vasgen", "Oslonbekovich", "Mirnyi", 1, 2016));
        db.addEmployee(new Employee("Akopyan", "Narek", "Michailovich", "Bugulma", 11, 2017));
        db.addEmployee(new Employee("Belov", "Aleksandr", "Sergeevich", "Chistopol", 1, 2015));
        db.addEmployee(new Employee("Dzuba", "Sergey", "Valentinovich", "Zainsk", 1, 2017));
        db.addEmployee(new Employee("Nagiev", "Dmitriy", "Borisovich", "Moscow", 4, 2015));
        db.addEmployee(new Employee("Androidov", "Iphone", "Asusovich", "New York", 8, 2016));
        List<Employee> expectedListYear = new ArrayList<>();
        expectedListYear.add(new Employee("Tolstoy", "Lev", "Ivanovich", "Zelenograd", 7, 2010));
        assertEquals("TaskCh13N12Test.testMoreYears", expectedListYear, db.searchWorkingMoreYears(7));
    }
}
